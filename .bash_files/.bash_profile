# Need to put usr/local/bin in front of /usr/bin in the path
homebrew=/usr/local/bin:/usr/local/sbin
export PATH=$homebrew:$PATH

# color command line
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

source /usr/local/git/contrib/completion/git-completion.bash
source /usr/local/git/contrib/completion/git-prompt.sh

export GIT_PS1_SHOWCOLORHINTS=1
export GIT_PS1_SHOWDIRTYSTATE=1
export PS1='\w$(__git_ps1 " \e[0;33m(%s)\e[m") > '
