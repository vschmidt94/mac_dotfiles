/w/w--------------------------------------------------------
" Vaughan's vimrc file for Mac
" Built for working with Linux Mint 18 as a virtual machine
"
" Maintainer:
" 	Vaughan Schmidt
" 	http://cvschmidt.me - vaughan.schmidt@cvschmidt.me
"
" Credits:
" 	Originally based upon Amir Salihefendic's
"       http://amix.dk/blog/post/19691#The-ultimate-Vim-configuration-on-Github
"
"       Additional settings liberated from
"       N. Santana
"       A. Matteson
"
"       Or elsewhere out in the wild.
"
" Sections:
"    -> General
"    -> Appearance
"    -> User Interface
"       -> General
"       -> Leader
"       -> Moving around
"       -> Search Options
"       -> Text Tabs and Indents
"    -> Plug-in settings
"       -> Airline
"       -> Autosave
"       -> Ctrl-P
"       -> Fugitive
"       -> Tagbar
"       -> YouCompleteMe
"
"-----------------------------------------------------------


"-----------------------------------------------------------
" General
"-----------------------------------------------------------
" Enable Pathogen plugin fist to load plugins
" Pathogen options --------------------------{{{
set nocompatible
runtime bundle/pathogen/autoload/pathogen.vim
execute pathogen#infect()
Helptags
" }}}

" Sets how many lines of history VIM has to remember
set history=700

" Enable filetype plugins
filetype plugin on
filetype indent on

" set line wrap
set wrap

" set spell checker
set spell

" Set to auto read when a file is changed from the outside
set autoread

"-----------------------------------------------------------
" Appearance
"-----------------------------------------------------------

" GUI / gvim options
if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window
  set lines=999 columns=999
  if has("win32")
      set guifont=Iosevka_Term:h11:cANSI
  else
    if has("unix")
        set guifont=Iosevka\ Term:h14
    endif
  endif
  " After much thrashing, I've settled on gruvbox colorscheme
  " https://github.com/morhetz/gruvboxcolorscheme gruvbox
  colorscheme gruvbox
  set background=dark
else
  " This is console Vim.
  if exists("+lines")
    set lines=50
  endif
  if exists("+columns")
    set columns=100
  endif
  if $COLORTERM == 'gnome-terminal' || $COLORTERM == 'mate-terminal' || $TERM == 'xterm-256color'
    "gnome-terminal can support the 256 color for gruvbox
    set t_Co=256
    colorscheme gruvbox
    set background=dark
  endif
endif
"Vertical lines - set up to match GRMN coding standard columns
set colorcolumn=25,37,61,71,81,93

" Change the appearance of the cursor in insert mode
"if has("autocmd")
"  au VimEnter,InsertLeave * silent execute '!echo -ne "\e[2 q"' | redraw!
"    au InsertEnter,InsertChange *
"        \ if v:insertmode == 'i' |
"        \   silent execute '!echo -ne "\e[6 q"' | redraw! |
"        \ elseif v:insertmode == 'r' |
"        \   silent execute '!echo -ne "\e[4 q"' | redraw! |
"        \ endif
"      au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
"  endif
"-----------------------------------------------------------
" User Interface: General
"-----------------------------------------------------------

" Displays relative line numbers, in relation to current
" current cursor line
set relativenumber
set number

" Show current position
set ruler

" Scroll offset keeps cursor from going all the way to top
" or bottom when moving vertically
set so=7

" Height of command bar
set cmdheight=2

" Always show the status line
set laststatus=2

"Format the status line
set statusline=\ %F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l

" Set buffer to hidden when it is abandoned
set hid

" Set backspace to work like we expect it to
set backspace=eol,start,indent

" Set to delete trailing spaces for certain file types on
" save.
autocmd BufWritePre *.c, *.cpp, *.h, *.hpp :%s/\s\+$//e

"-----------------------------------------------------------
" User Interface: Leader
"
" I use <space> as leader and <cr> as local leader
"-----------------------------------------------------------
" Leader lets you add additional key mappings
let g:mapleader="\<space>"
let mapleader="\<space>"
let maplocalleader="\<cr>"

"-----------------------------------------------------------
" User Interface: Moving around
"-----------------------------------------------------------

" Disable arrow keys because they are a crutch
nnoremap <Left> <nop>
nnoremap <Up> <nop>
nnoremap <Right> <nop>
nnoremap <Down> <nop>

" Disable arrow keys insert as well
inoremap <Left> <nop>
inoremap <Up> <nop>
inoremap <Right> <nop>
inoremap <Down> <nop>

" Easier split navigation
nnoremap <leader>j <C-W><C-j>
nnoremap <leader>k <C-W><C-k>
nnoremap <leader>l <C-W><C-l>
nnoremap <leader>h <C-W><C-h>

" Swap windows around using a similar motion as navigating, just hold shift
" during key command
nnoremap <leader>J <C-W><S-j>
nnoremap <leader>K <C-W><S-k>
nnoremap <leader>L <C-W><S-l>
nnoremap <leader>H <C-W><S-h>

" Treat long lines as break lines
"  (useful when moving around in them)
map j gj
map k gk

" Use jk as an escape combination in insert mode.
"  You never type jk together at same time, so it should
"  not come up in normal typing
inoremap jk <esc>

"-----------------------------------------------------------
" User Interface: Buffer Management
"-----------------------------------------------------------
" Close the current buffer
map <leader>bd :Bclose<cr>

" Show a list of buffers and wait on number to switch to
map <leader>bb :ls<cr>:b<Space>

"-----------------------------------------------------------
" User Interface: Search Options
"-----------------------------------------------------------

" Ignore case when searching
set ignorecase

" When searching, try to be smart about case. If the search
" pattern contains uppercase, then search will be case-
" sensitive
set smartcase

" Highlight search results
set hlsearch

" Show the next match while entering search
set incsearch

" Un-highlight with double <esc>
nnoremap <esc> :nohlsearch<cr>:<esc>

"-----------------------------------------------------------
" User Interface: Text, Tabs, and Indents
"-----------------------------------------------------------

" set textwidth to 120
set textwidth=100
" except for git commit files
au FileType gitcommit set textwidth=72

" Wrap lines
set wrap

" Use spaces instead of tabs
set expandtab
set smarttab

" 1 tab = 4 spaces
set shiftwidth=4
set tabstop=4

" Turn on autoindent and smart indent
set autoindent
set smartindent

"-----------------------------------------------------------
" Plug-In settings
"
" Plug-In's currently setup on my linux box:
"  - Airline
"  - Ctrl-P
"  - fugitive
"  - Gruvbox (appearance theme)
"  - NERDTree:
"     https://github.com/scrooloose/nerdtree.git
"  - Tagbar
"     https://github.com/majutsushi/tagbar.git
"-----------------------------------------------------------

"-----------------------------------------------------------
" Plug-in: Airline
"-----------------------------------------------------------
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tagbar#enabled=1
let g:airline#extensions#tagbar#flags='s'

"-----------------------------------------------------------
" Plug-in: Autosave
"-----------------------------------------------------------
" Enable Autosave on startup
let g:auto_save=1
" Disable in insert mode
let g:auto_save_in_insert_mode=0
" Silence the output
let g:auto_save_silent=1

"-----------------------------------------------------------
" Plug-in: Ctrl-P which for me is <space>f
"-----------------------------------------------------------
" I don't like ctrl, if i don't need it
" so remap CTRL-P to be easier to open with leader-f
let g:ctrlp_map = '<leader>f'

"ignore binaries
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn|sbas)$',
    \ 'file': '\v\.(lnk|ucfg|sln|o32|o|via|exe|so|dll|bmp|obj|rsp|oxml|sbmp|lib|pyc)$',
    \ }

"-----------------------------------------------------------
" Plug-in: Fugitive
"-----------------------------------------------------------
" Going to use the 'g' prefix to signify git commands
" So basically everything will be <leader>g<something>
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gc :Gcommit<CR>
nnoremap <leader>gl :Glog<CR>
nnoremap <leader>gd :Gvdiff<CR>
nnoremap <leader>gb :Gblame<CR>
nnoremap <leader>gm :Gvdiff master<CR>

" To prevent me accidentally making changes on the master branch I only
" blindly push branches if they aren't master.
" May need to update this to ignore 'development/**' and 'production/**'
nnoremap <leader>gp :call Safepush()<CR>
function! Safepush()
let l:branchname = fugitive#head()
if l:branchname ==? 'master'
    echo 'Not pushing, this is a mainline branch: "' . l:branchname . '"'
else
    execute ":Git push"
endif
endfunction

" Add the branch to the status line
set laststatus=2
set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %P

"-----------------------------------------------------------
" Plug-in: Tagbar
"-----------------------------------------------------------

"This allows exploring file by function variable declarations
"Use leader t to show the tagbar
nmap <F8> :TagbarToggle<CR>
nnoremap <leader>t :TagbarOpenAutoClose<CR>

"override the order of displayed info for my
"preferences
let g:tagbar_type_c = {
    \ 'kinds' : [
        \ 'f:functions:0:1',
        \ 'd:macros:0:0',
        \ 'g:enums',
        \ 'e:enumerators:0:0',
        \ 't:typedefs:0:0',
        \ 's:structs',
        \ 'u:unions',
        \ 'm:members:0:0',
        \ 'v:variables:0:0',
    \ ],
\ }

let g:tagbar_type_cpp = {
    \ 'kinds' : [
        \ 'f:functions:0:1',
        \ 'd:macros:0:0',
        \ 'g:enums',
        \ 'e:enumerators:0:0',
        \ 't:typedefs:0:0',
        \ 'n:namespaces',
        \ 'c:classes',
        \ 's:structs',
        \ 'u:unions',
        \ 'm:members:0:0',
        \ 'v:variables:0:0',
    \ ],
\ }

let g:tagbar_type_jam = {
    \ 'ctagstype' : 'jam',
    \ 'kinds'     : [
        \ 'r:rules',
        \ 'v:variables'
    \ ],
\ }

"-----------------------------------------------------------
" Plug-in: VimWiki
"-----------------------------------------------------------
let g:vimwiki_list = [
                        \{'path': '~/git/vimwiki_personal/vs_personal.wiki', 'syntax': 'markdown', 'ext': '.md'},
                        \{'path': '~/vimwiki/personal/vs_notebook.wiki'}
                     \]
au BufRead,BufNewFile *.wiki set filetype=vimwiki
:autocmd FileType vimwiki map d :VimwikiMakeDiaryNote
function! ToggleCalendar()
  execute ":Calendar"
  if exists("g:calendar_open")
    if g:calendar_open == 1
      execute "q"
      unlet g:calendar_open
    else
      g:calendar_open = 1
    end
  else
    let g:calendar_open = 1
  end
endfunction
:autocmd FileType vimwiki map c :call ToggleCalendar()

"-----------------------------------
" -> Plug-in: YouCompleteMe
"-----------------------------------
" (as with a lot of this, from Nick Santana's vimrc)
" I was having problems with it erroring out on
" vim scripts when editing so this just tells it
" to work on c files
"
let g:ycm_filetype_whitelist= {'c': 1, 'python': 1, 'cpp': 1}
let g:ycm_confirm_extra_conf = 0
let g:ycm_global_ycm_extra_conf = '~/.vim/clangflags.py'
let g:ycm_autoclose_preview_window_after_insertion = 1
"let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_always_populate_location_list = 1

nnoremap <localleader>] :YcmCompleter GoTo<CR>
nnoremap <localleader>t :YcmCompleter GetType<CR>

" YCM holds onto file longer than it should and sometimes prevents saves
" This is a hack to force a save.
function! SaveWithYCM()
YcmRestartServer
w!
endfunction

command! W call SaveWithYCM()



