## Vaughan's dotfile repo

True story - I was brave enough to do a fresh install of OSX on my Mac, after
having essentially the same computer for ~10 years. Sure, I've upgraded hardware
from time to time, but always just restored from TimeMachine with a new device.
This time, I decided to start completely fresh, and as I've been bringing this
"fresh" machine back online, I've discovered just how many tweaks and tunings I've
dialed in.  (Yes, I can dig into my backups to see exactly what I did previously -
but my goal is to only bring forward the stuff I actually use and want.)

In any event, I'm *attempting* to organize my dotfile collection in a better, more
organized way using this as a central repository and then creating symlinks where the
files would normally go.

Anyhoo... There's probably not much here useful to anyone other than me, but
don't let that stop you if there is something here that proves useful to you.
